# 도메인 정보 & 도큐멘트 폴더

### 실서버
- 울산 심플치과

  - path : /datamounted/clearalign_simple/www_20180720 
  - link : [simple.co.kr](http://simpledental.co.kr)

  

- 인천 투명플러스치과

  - path : /datamounted/clearalign_incheon/www_20170823 
  - link : [clearplus.co.kr](http://www.clearplus.co.kr/)

  

- 신규 콜 솔루션

  - path : /datamounted/deaz_call/html
  - link : [deaz.call.cleardental.co.kr](http://deaz.call.cleardental.co.kr/)

  

- 신규 랜딩 솔루션

  - path : /datamoutned/deaz_pro/html 
  - link : [deaz.pro.cleardental.co.kr](http://deaz.pro.cleardental.co.kr/)

  

- 랜딩 이벤트 페이지

  - path : /datamounted/Landing_Page_Datas
  - access : 랜딩솔루션->로그인->이벤트별관리->이벤트없음->경로테스트

- 디아즈

  - path : /home/gmlthd0908/newdeaz 
  - link : [deaz.co.kr](http://www.deaz.co.kr)

  


### 테스트서버

- t1 : [t1.deaz.co.kr](http://t1.deaz.co.kr)
- t3 : [t3.deaz.co.kr](http://t3.deaz.co.kr)
