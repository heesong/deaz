# 1. 기획자한테 소스를 받는다

# 2. head 파일을 연다

- 울산 심플치과 

path: /datamounted/cleardental_simple/www_20180720/include/head.inc.php

- 인천 투명플러스치과

path: /datamounted/cleardental_incheon/www_201800823/include/head.inc.php

# 3. 스크립트 소스를 붙여넣기 한다

`Facebook Pixel Code` 검색

```html
<!-- Facebook Pixel Code -->
	<script>
	!function(f,b,e,v,n,t,s)
	{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
	n.callMethod.apply(n,arguments):n.queue.push(arguments)};
	if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
	n.queue=[];t=b.createElement(e);t.async=!0;
	t.src=v;s=b.getElementsByTagName(e)[0];
	s.parentNode.insertBefore(t,s)}(window,document,'script',
	'https://connect.facebook.net/en_US/fbevents.js');
	fbq('init', '241672656462605'); 
	fbq('track', 'PageView');
	fbq('track', 'ViewContent');
	</script>
	<noscript>
	 <img height="1" width="1" 
	src="https://www.facebook.com/tr?id=241672656462605&ev=PageView
	&noscript=1"/>
	</noscript>
<!-- End Facebook Pixel Code -->
```
주석 부분 안에 내용 전체 교체


