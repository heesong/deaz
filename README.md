# 디아즈

Deaz daily work process



# 설치프로그램

- [VSCODE](https://code.visualstudio.com/docs/?dv=win)
- [파일질라](https://filezilla-project.org/download.php?type=client)



# VSCODE 필수 설치 플러그인 



- ftp-simple :

`ctrl + shift + p`  누르고, `config` 입력.

`username`, `password`키의 값은 공유한 구글 스프레드시트 참고.

아래 소스 코드 복사 , 붙여넣기:

```json
[
	{
		"name": "deaz",
		"host": "14.63.161.105",
		"port": 20021,
		"type": "ftp",
		"username": "",
		"password": "",
		"path": "/",
		"autosave": true,
		"confirm": false
	},
	{
		"name": "deaztest",
		"host": "14.49.37.212",
		"port": 20021,
		"type": "ftp",
		"username": "",
		"password": "",
		"path": "/",
		"autosave": true,
		"confirm": false
	}
]
```



# 파일질라 세팅

![파일질라세팅](https://66.media.tumblr.com/141168da2df09014c76d0201bac48d12/tumblr_pmueaj39PG1uddfylo1_540.jpg)

①`사이트관리자` -> ②`새 사이트` -> ③`정보입력`(공유한 구글 스프레드시트에 있음) -> ④`연결`



