> 심플치과 팝업 적용 좀 해주세요.



## 1) 파일질라로 팝업 이미지 넣기

- 파일질라 열고, 디아즈 접속
- 심플치과 접근
- `/img/main/popup/`경로에 팝업 이미지 파일 업로드





## 2) 심플치과 홈페이지 접속

`ctrl` + `shift` + `p` 입력 후,

`Remote directory open to work space ` 접속하고 `deaz` -> `datamounted` ->  `cleardental_simple` -> `www_20180720` -> `.`접속.



## 3) 팝업 파일 열기

- 경로 : `/include/popup_open_20190110.inc.php`
- 소스 :

```php
// 끄기
$popup_01 = false;

// 추가
$추가 = true;
if( isset($추가) && $추가){
	$div_name = "추가";
	$popup_value = array();
	$popup_value['popup_top'] = '0px';
	$popup_value['popup_left'] = '50%';
	$popup_value['popup_width'] = '600px';
	$popup_value['popup_height'] = 'auto';
	$popup_value['popup_max'] = '100%';
	$link = 'javascript:void(0)';
	$add_popup_style = "";

	if( CI_Controller_Revolution::$calledClassName && strtolower(CI_Controller_Revolution::$calledClassName)=='mobile' ){
		$popup_value['popup_top'] = '100px';
		$popup_value['popup_left'] = '50%';
		$popup_value['popup_width1'] = '50%';
		$popup_value['popup_max'] = '50%';
		$link = 'javascript:void(0)';
		$add_popup_style .= "text-align:center;position:fixed;";
	}
	$type_pp = '';
	$popup_tag = '<img src="이미지경로" /><iframe src="http://landing.deaz.co.kr/ntr3942776/186"></iframe><div class="popup_ctrl" style="position:relative;padding:12px 10px;background:rgba(255,255,255,0.8);"><label for="donshow_'.$div_name.'"><input type="checkbox" name="donshow_'.$div_name.'" id="donshow_'.$div_name. '" />하루 동안 보지 않기</label><p class="btn"><button onclick="popup_funcs.layer_close(\'' . $div_name . '\');$(this).closest(\'[id]\').hide();$(\'.overlay\').hide();">X</button></p></div>';
	$popup_tag = "<div class=\"overlay\"></div><div class=\"draggable ABA-web-editor\" id=\"{$div_name}\" style='display:none;z-index:999999;position:fixed;max-width:{$popup_value['popup_max']};left:{$popup_value['popup_left']};top:{$popup_value['popup_top']};width:{$popup_value['popup_width']};height:{$popup_value['popup_height']};{$type_pp};{$add_popup_style}'>{$popup_tag}</div>";
	$__popup_list[$div_name] = $popup_tag;
}
```

- 해야할 일 : 

  변수명 바꿔주기

  이미지 경로 입력하기



## 팝업 종류

1) 랜딩에서 가져오는 경우 :

랜딩 페이지 만들고 이벤트 등록 후,

소스 중 `<iframe></iframe>`부분에 만든 페이지 주소 경로를 붙여넣는다.



2) 이미지 파일인 경우 :

```php
$type_pp = '';
$popup_tag = '<div style="position:relative;display:inline-block;*display:inline;*zoom:1;box-shadow: 0px 0px 10px 5px rgba(0,0,0,0.1);"><a href="javascript:void(0)" style="display:block;line-height:0;"><img src="이미지경로" style="width:100%;max-width:100%;height:auto;" /></a><div class="pop_btn" style="position:relative;padding:12px 10px;background:rgba(255,255,255,0.8);font-size:11px;"><a href="javascript:void(0)" onclick="popup_funcs.layer_close(\''.$div_name.'\');$(this).closest(\'[id]\').hide()" style="float:right;">X</a><label style="width:100%;height:3%" for="donshow_'.$div_name.'"><input type="checkbox" name="donshow_'.$div_name.'" id="donshow_'.$div_name.'" style="width:7%;vertical-align: text-top;margin:0;"/>하루 동안 보지 않기</label></div></div>';
	$popup_tag = "<div class=\"draggable ABA-web-editor\" id=\"{$div_name}\" style='display:none;z-index:999;position:fixed;max-width:{$popup_value['popup_max']};left:{$popup_value['popup_left']};top:{$popup_value['popup_top']};width:{$popup_value['popup_width']};height:{$popup_value['popup_height']};{$type_pp};{$add_popup_style}'>{$popup_tag}</div>";
$__popup_list[$div_name] = $popup_tag;
```

`$type_pp`로 시작하는 부분을 위의 소스로 대체한다.

똑같이 `이미지경로` 부분에 해당 이미지 파일을 업로드한 경로를 붙여넣는다.

