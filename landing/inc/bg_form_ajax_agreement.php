<div class="form_inner"><form id="form-subscribe" method="post" onsubmit="form_ajax(this);return false;" action="/<?php echo $prefixuri;?>">
    <input type="hidden" name="regist" value=""><input type="hidden" name="content" placeholder="상세내용" value=" ">
	<div class="form_user_data">
		<!-- 이름 -->
		<div class="w-input w-input-name"><input type="text" name="name" class="input-name inp" autocomplete="off" required></div>
		<!-- 나이 -->
		<div class="w-input w-input-age"><input type="tel" name="age" class="input-age inp" maxlength="2" nextfield="phone2" autocomplete="off" required></div>
		<!-- 연락처 -->
		<div class="w-input w-input-contact w-input-contact3n">
			<div class="w-input w-input-phone w-input-phone1"><select class="input-phone0 phone" name="phone1">
				<option value="010">010</option>
				<option value="011">011</option>
				<option value="017">017</option>
			</select></div>
			<div class="w-input w-input-phone w-input-phone2"><input type="tel" name="phone2" class="input-phone1 phone" maxlength="4" nextfield="phone3" placeholder="" value="" minlength="4" required></div>
			<div class="w-input w-input-phone w-input-phone3"><input type="tel" name="phone3" class="input-phone2 phone" maxlength="4" nextfield="privacy-agree" placeholder="" value="" minlength="4" required></div>
		</div>
		<div class="form-agree-group text-center">
			<label for="privacy-agree"><input type="checkbox" id="privacy-agree" name="privacy-agree" required checked/>개인정보 수집 및 사용에 동의합니다.</label>
			<a class="btn_view_agreelaw">[자세히 보기]</a>
		</div>
		<img src="./image/jutout_20171107_form.jpg" class="bg">
	</div>
	<div class="agreement">
		<div class="privacy_wrap"><a href="javascript:void(0);" onclick="window.open('http://incheon.cleardental.co.kr/flow/?what=law.privacy', '개인정보취급방침', 'width=400,height=300,top=383,left=512,menubar=no,status=no,toolbar=no')" target="_blank">개인정보 취급방침 보기</a></div>
		<div class="agreement_content">
			<span>개인정보수집정책</span>
			<ol>
			<li>개인정보 수집 및 이용</li>
			<li>개인정보 수집주제 : 투명플러스치과</li>
			<li>개인정보 수집항목 : 이름, 나이, 연락처</li>
			<li>개인정보 수집 및 이용목적 : 투명플러스치과에서 치아교정 상담 활용 (전화, 문자)</li>
			<li>개인정보 보유 및 이용기간 : 수집일로부터 6개월 (고객 동의 철회시 지체없이 파기)</li>
			</ol>
		</div>
		<div class="agreement_content">
			<span>개인정보 취급 위탁</span>
			<ol>
			<li>개인정보 취급 위탁을 받는자 : (주)디아즈</li>
			<li>개인정보 취급 위탁을 하는 업무의 내용 : 고객정보 저장 및 서버관리</li>
			<li>상기 동의를 거부할 권리가 있으나, 수집 및 이용에 동의하지 않을 경우 치아교정 상담 및 이벤트 참여가 불가능 합니다.</li>
			</ol>
		</div>
	</div>
	<div class="wrap-submit-button"><div class="submit-button"><input type="submit" value="신청하기"></div><img src="./image/jutout_20171107_btn.jpg" class="bg"></div>
</form></div>

<style>
.w-input{width:59%;height:10%;left:33.5%;z-index:2;}
.w-input-name{top:20%;}
.w-input-age{top:44%;}
.w-input-contact{top:67%;}
.w-input-contact .w-input{width:27% !important;}
.w-input .w-input-phone{top:0;left:0;width:100%;height:100%;background:transparent;}
.form-agree-group{top:95%;height:5%;}
.form-agree-group label{margin-left:24.5%;width:34.5%;height:100%;}
.form-agree-group label input{width: 10%;height: 100%;}
.form-agree-group a.btn_view_agreelaw{width: 12%;right: 28%;}
.wrap-submit-button{position:relative;}
.submit-button{top: 19%;left: 6%;width: 88%;height: 61%;overflow: hidden;z-index: 2;}
.agreement{display:none;position:relative;width:84%;margin:2.5% auto;padding:10px;border:1px solid #c2c2c2;border-radius:5px;text-align:left;}
.privacy_wrap{text-align:right;}
.privacy_wrap img{display:inline;}
.agreement_content {text-align: left;font-size: 12px;line-height: 1.5;margin-bottom: 20px;}
.agreement_content span{font-weight:bold;}
.agreement_content ol li{text-align:left;}
.privacy_wrap a{display: inline-block;font-size: 12px;text-decoration: none;color: #000;font-weight: bold;background: #ccc;padding: 5px;letter-spacing: -1.5px;}
</style>
<script type="text/javascript">
$('.btn_view_agreelaw').click(function(){
	$('.agreement').toggle();
	return false;
});
</script>