<?php if(0){?><div class="form-inline">
    <form class="form-container" method="post" onsubmit="form_ajax(this);return false;" action="/<?php echo $prefixuri;?>">
        <fieldset>
            <input type="hidden" name="regist" value="">
            <!-- 이름-->
            <div class="form-group">
                <div class="label">이름</div>  
                <input type="text" name="name" placeholder="이름" required>

            </div>
            <!--나이-->
            <div class="form-group">
                <div class="label">나이</div> 
                <input type="tel" name="age" placeholder="나이" maxlength="2" nextfield="[name='phone2']">
            </div>
            <!-- 연락처 -->
            <div class="form-group">
                <div class="label">연락처</div>
                <select class="phn_01" name="phone1" style="height: 30px;">
                    <option value="010">010</option>
                    <option value="011">011</option>
                    <option value="017">017</option>
                </select>
                -<input class="phn_02" type="tel" name="phone2" maxlength="4" nextfield="[name='phone3']" minlength="4">
                -<input class="phn_03" type="tel" name="phone3" maxlength="4" required minlength="4">
            </div>

            <div class="form-group">
                <div class="agreement">
                    [개인정보취급방침] 
                    동의           
                    <input class="btn_privacy" type="checkbox" checked="checked" required>
                </div>
                <a style="cursor: pointer;" onclick="window.open('http://ulsan.cleardental.co.kr/flow/?what=law.privacy', '개인정보취급방침', 'width=400,height=300,top=383,left=512,menubar=no,status=no,toolbar=no')"><img src="/image/btn_privacy.png"></a>
            </div> 
            <div class="submit-button">
                <div><input type="submit" value="신청하기"></div>
            </div>
        </fieldset>
    </form>
</div><?php }?>
<style>
.inner_form_wrap-nobg .w-input{margin:0;padding:0;padding-bottom:10px;text-align:left;max-width:none;margin-right:20%;}
.inner_form_wrap-nobg .w-input .label{display:inline-block;*display:inline;*zoom:1;font-size:3vh;color:#fff;width:15%;text-align:left;font-weight:bold;}
.inner_form_wrap-nobg .w-input input, .inner_form_wrap-nobg .w-input select{font-size:3vh;text-align:left;height:30px;width:75%;padding:5px;border-radius:2px;border:0;background:#fff;}
.inner_form_wrap-nobg .w-input select{box-sizing:content-box;}
.inner_form_wrap-nobg .form-agree-group {width:auto;margin-right:20%;padding-right:7.5%;text-align:right;}
.inner_form_wrap-nobg .form-agree-group *{font-size:2vh;color:#fff;vertical-align:top !important;line-height:2vh;}
.inner_form_wrap-nobg .form-agree-group label input{width:20px;height:20px;margin-right:5px;}
.inner_form_wrap-nobg .form-agree-group a{position:static !important;display:inline-block;*display:inline;*zoom:1;opacity:1 !important;color:#ffcd00;vertical-align:middle;overflow:visible !important;}
.inner_form_wrap-nobg .form-agree-group label{display:inline-block;*display:inline;*zoom:1;font-size:2vh;}
.inner_form_wrap-nobg{display:block;}
.inner_form_wrap-nobg .form{position:relative;}
.inner_form_wrap-nobg .w-input{position:static;}
.inner_form_wrap-nobg .w-input{margin-right:20%;text-align:left;}
.inner_form_wrap-nobg .w-input-phone{display:inline-block;*display:inline;*zoom:1;padding:0;margin:0;width:23%;margin-right:3%;}
.inner_form_wrap-nobg .w-input-phone3{margin:0;}
.inner_form_wrap-nobg .w-input-phone input, .inner_form_wrap-nobg .w-input-phone select{width:100%;}
.inner_form_wrap-nobg .form-agree-group{position:static;}
.inner_form_wrap-nobg .submit-button{top:5%;left:auto;right:0%;width:20%;height:90%;}
.inner_form_wrap-nobg input[type="submit"] {
    width:80px;
    height:80px;
/*    border-radius:100%;*/
    background-color: #ffffff;
    color: #000;
/*    border-radius: 50%;*/
    font-size: 15px;
    font-weight:600;
    margin:0 auto;
    min-width: 80px;
    padding: 0;
    overflow: hidden;
    box-shadow: 0 1px 1.5px 0 rgba(0,0,0,.12), 0 1px 1px 0 rgba(0,0,0,.24);
    position: relative;
    line-height: normal;
    transition: all 1s;
}
.inner_form_wrap-nobg .submit-button input{width:100%;height:100%;font-size:4vh;border-radius:2px;overflow:hidden;transition:none;}
</style>
<div class="inner_form_wrap inner_form_wrap-nobg">
<div class="w-bg_form"><div class="form"><form id="form-subscribe" method="post" onsubmit="form_ajax(this);return false;" action="/<?php echo $prefixuri;?>">
    <input type="hidden" name="regist" value="">
    <input type="hidden" name="content" placeholder="상세내용" value=" ">
	<!-- 이름 -->
	<div class="w-input w-input-name"><label class="label">이름</label><input type="text" name="name" class="input-name inp" autocomplete="off" required></div>
	<!-- 나이 -->
	<div class="w-input w-input-age"><label class="label">나이</label><input type="tel" name="age" class="input-age inp" maxlength="2" nextfield="phone2" autocomplete="off" required></div>
	<!-- 연락처 -->
	<div class="w-input w-input-contact w-input-contact3n">
		<label class="label">연락처</label>
		<div class="w-input w-input-phone w-input-phone1"><select class="input-phone0 phone" name="phone1">
			<option value="010">010</option>
			<option value="011">011</option>
			<option value="017">017</option>
		</select></div>
		<div class="w-input w-input-phone w-input-phone2"><input type="tel" name="phone2" class="input-phone1 phone" maxlength="4" nextfield="phone3" placeholder="" value="" minlength="4" required></div>
		<div class="w-input w-input-phone w-input-phone3"><input type="tel" name="phone3" class="input-phone2 phone" maxlength="4" nextfield="privacy-agree" placeholder="" value="" minlength="4" required></div>
	</div>
	<div class="form-agree-group text-center">
		<label for="privacy-agree"><input type="checkbox" id="privacy-agree" name="privacy-agree" required checked/>개인정보 수집 및 사용에 동의합니다.</label>
		<a class="btn_view_agreelaw" style=""  onclick="window.open('http://incheon.cleardental.co.kr/flow/?what=law.privacy', '개인정보취급방침', 'width=400,height=300,top=383,left=512,menubar=no,status=no,toolbar=no')">[자세히 보기]</a>
		<div class="submit-button"><input type="submit" value="신청하기"></div>
	</div>
</form></div></div>
</div>