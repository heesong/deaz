<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$uniqueid = uniqid();
?>
<!DOCTYPE html>
<html lang="kor">
<head>
	<meta charset="utf-8">
	<meta name="viewPort" content="initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no" />
	<title>투명플러스치과</title>
	<script src="http://code.jquery.com/jquery-1.12.4.min.js"></script>
	<script src="http://code.jquery.com/ui/1.10.0/jquery-ui.js"></script>
	<link rel="stylesheet" href="/css/reset.css" type="text/css" media="all" />
	<link rel="stylesheet" href="/css/common.css" type="text/css" media="all" />
	<link rel="stylesheet" href="http://code.jquery.com/ui/1.8.18/themes/base/jquery-ui.css" type="text/css" media="all" />
	<!-- Facebook Pixel Code -->
	<script>
	  !function(f,b,e,v,n,t,s)
	  {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
	  n.callMethod.apply(n,arguments):n.queue.push(arguments)};
	  if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
	  n.queue=[];t=b.createElement(e);t.async=!0;
	  t.src=v;s=b.getElementsByTagName(e)[0];
	  s.parentNode.insertBefore(t,s)}(window, document,'script',
	  'https://connect.facebook.net/en_US/fbevents.js');
	  fbq('init', '2101141136802305');
	  fbq('track', 'PageView');
	  fbq('track', 'ViewContent');
	</script>
	<noscript><img height="1" width="1" style="display:none"
	  src="https://www.facebook.com/tr?id=2101141136802305&ev=PageView&noscript=1"
	/></noscript>
	<!-- End Facebook Pixel Code -->
	<!-- Kakao Pixel Code-->
	<script type="text/javascript" charset="UTF-8" src="//t1.daumcdn.net/adfit/static/kp.js"></script>
	<!-- End Kakao Pixel Code-->
</head>
<body>
<div id="Wrap"><div id="Wrap_inner">
<div id="loading">
	<div class="tb-tb">
		<div class="tb-tr">
			<div class="tb-td">
				<p style="font-size:3vh;letter-spacing:1vh;margin-bottom:5%;">Loading.</p>
				<div class="loader-img"><div></div></div>
			</div>
		</div>
	</div>
</div>
<div id="Wrap_container">