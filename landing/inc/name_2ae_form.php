<style>
.w-bg_form .w-input-age{display:none;}
</style>
<div class="inner_form_wrap">
<div class="w-bg_form"><div class="form"><form id="form-subscribe" method="post" onsubmit="form_ajax(this);return false;" action="/<?php echo $prefixuri;?>">
    <input type="hidden" name="regist" value="">
    <input type="hidden" name="content" placeholder="상세내용" value=" ">
	<!-- 이름 -->
	<div class="w-input w-input-name"><input type="text" name="name" class="input-name inp" autocomplete="off" required></div>

	<div class="w-input w-input-name2"><input type="text" name="name2" class="input-name2 inp" autocomplete="off" nextfield="phone2" required></div>
	<!-- 나이 -->
	<div class="w-input w-input-age"><input type="tel" name="age" class="input-age inp" maxlength="2" autocomplete="off" required></div>
	<!-- 연락처 -->
	<div class="w-input w-input-contact w-input-contact3n">
		<div class="w-input w-input-phone w-input-phone1"><select class="input-phone0 phone" name="phone1">
			<option value="010">010</option>
			<option value="011">011</option>
			<option value="017">017</option>
		</select></div>
		<div class="w-input w-input-phone w-input-phone2"><input type="tel" name="phone2" class="input-phone1 phone" maxlength="4" nextfield="phone3" placeholder="" value="" minlength="4" required></div>
		<div class="w-input w-input-phone w-input-phone3"><input type="tel" name="phone3" class="input-phone2 phone" maxlength="4" nextfield="privacy-agree" placeholder="" value="" minlength="4" required></div>
	</div>
	<div class="form-agree-group text-center">
		<label for="privacy-agree"><input type="checkbox" id="privacy-agree" name="privacy-agree" required checked/>개인정보 수집 및 사용에 동의합니다.</label>
		<a class="btn_view_agreelaw" style=""  onclick="window.open('http://incheon.cleardental.co.kr/flow/?what=law.privacy', '개인정보취급방침', 'width=400,height=300,top=383,left=512,menubar=no,status=no,toolbar=no')">[자세히 보기]</a>
		<div class="submit-button"><input type="submit" value="신청하기"></div>
	</div>
</form></div><img src="/image/bg_form.jpg" class="bg" /></div>
</div>