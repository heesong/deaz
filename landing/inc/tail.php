<?php if( 1 && !defined('NoCommonInfo') ){?><div><img class="tail_info" src="/image/common_info.jpg?v=20171017"></div><?php } ?></div></div></div>

<!-- Mobon Tracker v3.0 [공용] start -->
<script type="text/javascript">
        function mobRf(){
                var rf = new EN();
                rf.setSSL(true);
                rf.sendRf();
				console.log("모비온공통");
        }
        function mobConv(){
			var cn = new EN();
			cn.setData("uid", "clearincheon");
			<?php if (property_exists($this, 'landinginfo') && is_array($this->landinginfo) && array_key_exists('pageno', $this->landinginfo)) { ?>
			cn.setData("ordcode", "<?php echo $this->landinginfo['pageno']; ?>");
			<?php } else { ?>
			cn.setData("ordcode", "");
			<?php } ?>
			cn.setData("qty", "1");
			cn.setData("price", "1");
			cn.setData("pnm", encodeURIComponent(encodeURIComponent("counsel")));
			cn.setSSL(true);
			cn.sendConv();
			console.log("모비온전환");
    }
</script>
<script async="true" src="https://cdn.megadata.co.kr/js/enliple_min2.js" onload="mobRf()"></script>
<!-- Mobon Tracker v3.0 [공용] end -->
<script src="/js/landing.js?v=20170823"></script>
<script type="text/javascript">
    if( typeof window.form_ajax!=="function" ){
        window.form_ajax = function(f){
            var $f = $(f), valid=true;

            $f.find('input, textarea').each(function(){
                if( !$(this).val() && $(this).attr('required') ){
                    valid = false; 
                    return false;
                }
                if( $(this).attr('name') == 'age' && $(this).val() < 19 ){
                    valid = false;
                    return false;
		}
            });
            if( !valid ) return false;            
            
            $.ajax({
                url: $f.attr('action')||document.location.href
                , method: 'post'
		, dataType: 'json'
                , data: $f.serialize()                
            }).done(function( data ) {
                var rs=data&&data.rs, cburi, cbscript;
                if( rs==0 ){
                    if( data.cburi ) cburi = data.cburi;
                    if( data.cbscript ) cbscript = data.cbscript;
                    if( cbscript && typeof cbscript=='object' ){
			var script, fn, key;
                        for( key in cbscript ){
                            script = cbscript[key];
                            script = script.replace(/(<script\b[^>]*>|<\/script>)/gm,'');
                            fn = new window.Function(script);
                            try{fn.apply(window);}catch(e){}
                       }
                    }

                    try{
                        //add_wcs(4, "0::${pageno}::__IP__::"+data.name+"::"+(data.phone?data.phone:(data.phone1+data.phone2+data.phone3)));
                        __add_wcs("${pageno}");
                        add_wcs(4, "200${pageno}");
                    }catch(e){}
                    data.msg && alert(data.msg);
                    try{mobConv();}catch(e){}

                    if( cburi ) location.href = cburi;
                    else location.reload();
                    
                }else alert(data.msg||'등록에 실패했습니다. 문제가 지속되면 관리자에게 문의하세요');
            });
        };
    }    
</script>
<script type="text/javascript">
<?php 
	if( in_array($_SERVER['REMOTE_ADDR'], explode(",",'211.37.79.174,123.215.23.72')) && isset($_GET['fast']) ){
?>
window.realloadevent = window.realloadevent || function(){ $('#Wrap_container').show(); }
$('#loading').stop(true,true).hide();
window.realloadevent();
$(window).on('load', function(){
	$('#loading').stop(true,true).hide('fade', 300, function(){
		try{
			window.realloadevent();
		}catch(e){ console.log(e) }
	});
});
<?php } else { ?>
window.realloadevent = window.realloadevent || function(){ $('#Wrap_container').show('fade', 1); }
$(window).on('load', function(){
	$('#loading').stop(true,true).hide('fade', 300, function(){
		try{
			window.realloadevent();
		}catch(e){ console.log(e) }
	});
});
<?php } ?>
</script>
<script type="text/javascript">
	kakaoPixel('2392957642272323732').pageView('PV');
</script>
</body>
</html>