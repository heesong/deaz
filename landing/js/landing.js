var resizetimer, fn_fit_windowheight = function(){
	var $body = $('body'), $win=$(window)
		, $contents = $body.find('[fitheight]')
		, $cur
		, ww=$win.width(), wh=$win.height(), w, h, rw, rh
		, ratew, rateh, isfirst=true
	;

	$contents.each(function(){
		$cur = $(this);
		w = $cur.width();
		h = $cur.height();
		if( ratew===undefined ){
			rateh = wh/h;
			ratew = w*rateh/w;
		}
		rw=w*ratew;
		rh=h*rateh;
		if( 0 && rw>ww ) return;
		$cur.width(rw);
		if( isfirst ) $cur.height(rh);
		isfirst = false;
	});
}
$(function(){
	$('form').on('submit', function(){
		// facebook pixel lead
		fbq('track', 'Lead');
		// Kakao Pixel
		kakaoPixel('2392957642272323732').completeRegistration('DB');

		var $f = $(this), $age=$f.find('[name="age"]'), age=$age.val();
		if( $age.length===1 ){
			if( age<19 ){
				alert('만 19세이상부터 신청가능합니다.');
				return false;
			}
		}
		
		var minlengthvalid = true; 
		$minlengths = $f.find('[minlength]');
		$minlengths.each(function(){
			var $me = $(this), min=Number($(this).attr('minlength')), val = $me.val();
			if( isNaN(min) ) return;
			if( min>val.length ){
				alert('최소 ' + min + '자를 입력하셔야 합니다.' );
				$me.focus();
				minlengthvalid = false;
				return false;
			}
		});
		if( !minlengthvalid ) return false;
		return true;
	});
	//	for multiple form
	$('label[for="privacy-agree"] input').on('click', function(){
		var $me = $(this), checked=false;
		if( $me.is('label[for="privacy-agree"]') ) $me = $me.find('input');

		$('label[for="privacy-agree"] input').each(function(){
			$(this).get(0).checked = !!$me.get(0).checked;
		})
	});
	//	for move focus input to next input automatically 
	$('input[nextfield]').on('keyup.keyup-event-for-autonextfield', function () {
		var $me = $(this), $form = $me.closest('form'),
			name = $me.attr('name'), next = $me.attr('nextfield'),
			max = 0,
			cur = $me.val().length
		;
		
		if ( next && (max = $me.attr('maxlength')) > 0 && cur >= max) {
			$me.val( $me.val().substring(0,max) );
			if( next.indexOf('[')!==0 ) next = "[name='" + next + "']";
			$form.find(next).focus();			
		}
	});
});
;(function(global, $){
	var fn_isNumberEvent= function(e, etc){
		var e=e||window.event||false, key=e.which||e.keyCode||true, keychar, etc=(typeof etc!=="boolean"&&true)||etc, shift=e.shiftKey||false
			, numstring="0123456789"
			, spkeys={'backspace': 8, 'tab': 9, 'enter': 13, 'end': 35, 'home': 36, 'left': 37, 'up': 38, 'right': 39, 'down': 40, 'insert': 45, 'delete': 46, 'numlock': 144, 'null': null}
			, etckeys=[], numkeys=[]
			, inp=e.target, format;
		if(key===true) return true;
		keychar = String.fromCharCode(key);
		if( etc ) for( var i in spkeys ) etckeys.push(spkeys[i]);
		numkeys=numkeys.concat(fn_range(48, 57));
		if( shift && numkeys.indexOf(key)>-1 ) return false;
		numkeys=numkeys.concat(fn_range(96, 105));
		var accesscodes=numkeys.concat(etckeys);
		if( accesscodes.indexOf(key)>-1 ) return true;
		else if (numstring.indexOf(keychar)>-1) return true; 
		if( e.target && (format=e.target.getAttribute('format')) ){
			if( (/^\-number$/).test(format) ){
				if( inp.selectionStart!==0 ) return false;
				if( !shift && (key===109 || key===189) ) return true;
			}
		}
		return false; 
	}, fn_range= function(a, b, step){
		var A= [];
		if(typeof a== 'number'){
			A[0]= a;
			step= step || 1;
			while(a+step<= b){
				A.push((a+= step));
			}
		}
		else{
			var s= 'abcdefghijklmnopqrstuvwxyz';
			if(a=== a.toUpperCase()){
				b=b.toUpperCase();
				s= s.toUpperCase();
			}
			s= s.substring(s.indexOf(a), s.indexOf(b)+ 1);
			A= s.split('');        
		}
		return A;
	}
	$('form').find('input[number], input[type="phone"], input[type="number"], input[type="tel"]').css('ime-mode','disabled').off('keydown.check_number_event keyup.check_number_event keypress.check_number_event').on('keydown.check_number_event keyup.check_number_event keypress.check_number_event', function(event){
		if( !fn_isNumberEvent(event) ){
			event.preventDefault();
		}
	}).on('blur.check_number_event', function(){
      var $me=$(this), val=$me.val();
      if( !(/^[0-9]*$/).test(val) ) $me.val('');
	}).end().find('input, select').on('change click touch blur keyup', function(){
		var 
			$form=$('form'), $me=$(this), name=$me.attr('name'), val=$me.val()
		;

		if( $me.attr('type')=='radio' ){
			$form.find('input[name="'+name+'"][value="'+val+'"]').not($me).each(function(){
				$(this).get(0).checked = true;
			});
		}else if( $me.attr('type')=='checkbox' ){
			$form.find('input[type="checkbox"][name="'+name+'"]').not($me).each(function(){
				$(this).get(0).checked = !!$me.get(0).checked;
			});
		}else{
			$form.find('input[name="'+name+'"], select[name="'+name+'"]').not($me).val(val);
		}
	});
}(window, jQuery));
$(window).resize(function(){
	try{
		if( resizetimer ) window.clearTimeout(resizetimer);
		resizetimer = window.setTimeout(function(){
			fn_fit_windowheight();
		}, 500);
	}catch(e){}
});
$(window).load(function(){
	$(window).resize();
});